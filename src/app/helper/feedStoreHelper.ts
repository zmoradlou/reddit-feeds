import {FeedModel} from "../model/feedModel";

export class FeedStoreHelper {
  limit: number;
  count: number;
  subreddit: string;
  feed: FeedModel;
}
