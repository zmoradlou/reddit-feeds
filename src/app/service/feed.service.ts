import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";

@Injectable()
export class FeedService {
  url: string = "https://www.reddit.com";

  constructor(public httpClient: HttpClient) {}

  getRedditFeeds(subReddit, limit, before, after) {
    return this.httpClient.get(
      this.url +
        "/r/" +
        subReddit +
        ".json?limit=" +
        limit +
        "&before=" +
        before +
        "&after=" +
        after
    );
  }

  getRedditPost(permalink) {
    return this.httpClient.get(this.url + permalink + ".json");
  }
}
