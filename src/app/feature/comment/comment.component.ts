import {Component, Input} from "@angular/core";
import {Helper} from "src/app/helper/helper";
import {PostModel} from "src/app/model/postModel";

@Component({
  selector: "app-comment",
  templateUrl: "./comment.component.html",
  styleUrls: ["./comment.component.css"]
})
export class CommentComponent {
  myHelper = Helper;

  @Input() comment: PostModel;
  constructor() {}
}
