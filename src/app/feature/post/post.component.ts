import {Component, OnInit} from "@angular/core";
import {ActivatedRoute} from "@angular/router";
import {FeedService} from "src/app/service/feed.service";
import {PostModel} from "src/app/model/postModel";
import {Helper} from "src/app/helper/helper";
import {FeedChildModel} from "src/app/model/feedChildModel";

@Component({
  selector: "app-post",
  templateUrl: "./post.component.html",
  styleUrls: ["./post.component.css"]
})
export class PostComponent implements OnInit {
  permalink: string;
  post = new PostModel();
  myHelper = Helper;
  commentsArray: Array<FeedChildModel> = [];

  constructor(private activatedRoute: ActivatedRoute, private feedService: FeedService) {}

  ngOnInit() {
    this.activatedRoute.params.subscribe(params => {
      this.permalink = params["link"];
      this.fetchPostData();
    });
  }

  fetchPostData() {
    this.feedService.getRedditPost(this.permalink).subscribe((res: any) => {
      Object.assign(this.post, res[0].data.children[0].data);
      Object.assign(this.commentsArray, res[1].data.children);
    });
  }
}
