import {FeedChildModel} from "./feedChildModel";

export class PostModel {
  id: string;
  name: string;
  thumbnail: string = "default";
  created: string;
  num_comments: number;
  author: string;
  score: number;
  permalink: string;
  title: string;
  selftext: string;
  body: string;
  body_html: string;
  replies: any;
}
