import {Component, OnInit} from "@angular/core";
import {FeedService} from "src/app/service/feed.service";
import {FeedModel} from "src/app/model/feedModel";
import {Router, ActivatedRoute} from "@angular/router";
import {Helper} from "src/app/helper/helper";
import {FeedStoreHelper} from "src/app/helper/feedStoreHelper";
import {DataStoreService} from "src/app/service/data-store.service";

@Component({
  selector: "app-feeds",
  templateUrl: "./feeds.component.html",
  styleUrls: ["./feeds.component.css"]
})
export class FeedsComponent implements OnInit {
  subReddit: string = "sweden";
  limit: number = 10;
  feed = new FeedModel();
  noFeedsFound = false;
  count: number = 0;
  myHelper = Helper;

  constructor(
    private feedService: FeedService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private dataStoreService: DataStoreService
  ) {}

  ngOnInit() {
    this.dataStoreService.storedFeed.subscribe(storedFeed => {
      if (storedFeed != null) {
        this.feed = storedFeed.feed;
        this.limit = storedFeed.limit;
        this.count = storedFeed.count;
        this.subReddit = storedFeed.subreddit;
      } else {
        this.fetchFeedData();
      }
    });
  }

  fetchFeedData() {
    this.feedService
      .getRedditFeeds(this.subReddit, this.limit, this.feed.before, this.feed.after)
      .subscribe(
        (res: any) => {
          if (res.data.children.length > 0) {
            this.noFeedsFound = false;
            this.feed = res.data;
            this.feed.after = res.data.children[res.data.children.length - 1].data.name;
            this.feed.before = res.data.children[0].data.name;
          } else {
            this.noFeedsFound = true;
          }
        },
        error => {
          if (error.status == 404) {
            this.noFeedsFound = true;
          }
        }
      );
  }

  changeTopic() {
    this.feed = new FeedModel();
    this.count = 0;
    this.fetchFeedData();
  }

  changeLimit() {
    this.feed = new FeedModel();
    this.count = 0;
    this.fetchFeedData();
  }

  goToNext() {
    this.count = this.count + this.limit;
    this.feed.before = null;
    this.fetchFeedData();
  }

  gotToPrevious() {
    this.count = this.count - this.limit;
    this.feed.after = null;
    this.fetchFeedData();
  }

  goToPost(permalink) {
    let currentFeed = new FeedStoreHelper();
    currentFeed.feed = this.feed;
    currentFeed.limit = this.limit;
    currentFeed.count = this.count;
    currentFeed.subreddit = this.subReddit;
    this.dataStoreService.setFeedData(currentFeed);
    this.router.navigate(["/post/", permalink]);
  }
}
