import {FeedChildModel} from "./feedChildModel";

export class FeedModel {
  before: string = null;
  after: string = null;
  children: Array<FeedChildModel> = [];
  dist: number;
}
