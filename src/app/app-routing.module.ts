import {NgModule} from "@angular/core";
import {Routes, RouterModule} from "@angular/router";
import {FeedsComponent} from "./feature/feeds/feeds.component";
import {PostComponent} from "./feature/post/post.component";

const routes: Routes = [
  {path: "feeds", component: FeedsComponent},
  {path: "post/:link", component: PostComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
