import {Injectable} from "@angular/core";
import {BehaviorSubject} from "rxjs";
import {FeedStoreHelper} from "../helper/feedStoreHelper";

@Injectable()
export class DataStoreService {
  private feedData = new BehaviorSubject<FeedStoreHelper>(null);

  public storedFeed = this.feedData.asObservable();

  setFeedData(feed) {
    this.feedData.next(feed);
  }

  constructor() {}
}
