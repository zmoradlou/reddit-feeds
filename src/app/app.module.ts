import {BrowserModule} from "@angular/platform-browser";
import {NgModule} from "@angular/core";
import {AppRoutingModule} from "./app-routing.module";
import {AppComponent} from "./app.component";
import {HttpClientModule} from "@angular/common/http";
import {FeedService} from "./service/feed.service";
import {FeedsComponent} from "./feature/feeds/feeds.component";
import {PostComponent} from "./feature/post/post.component";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {CommentComponent} from "./feature/comment/comment.component";
import {DataStoreService} from "./service/data-store.service";

@NgModule({
  declarations: [AppComponent, FeedsComponent, PostComponent, CommentComponent],
  imports: [BrowserModule, AppRoutingModule, FormsModule, ReactiveFormsModule, HttpClientModule],
  exports: [CommentComponent],
  providers: [FeedService, DataStoreService],
  bootstrap: [AppComponent]
})
export class AppModule {}
