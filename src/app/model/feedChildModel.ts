import {PostModel} from "./postModel";

export class FeedChildModel {
  data: PostModel = new PostModel();
  kind: string;
}
